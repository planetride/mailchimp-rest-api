<?php

namespace VPS;

class MailChimp {

    private $apiKey;
    private $apiUrl = 'https://<dc>.api.mailchimp.com/3.0/';

    /**
     * Create a new instance
     * @param string $apiKey - Optional
     */
    public function __construct($apiKey = false) {
        if ($apiKey) {
            $this->apiKey = $apiKey;
            list(, $datacentre) = explode('-', $this->apiKey);
            $this->apiUrl = str_replace('<dc>', $datacentre, $this->apiUrl);
        }
    }

    /**
     * Method to Set Api Key
     * @param string $apiKey
     */
    public function setApiKey($apiKey) {
        $this->apiKey = $apiKey;
        list(, $datacentre) = explode('-', $this->apiKey);
        $this->apiUrl = str_replace('<dc>', $datacentre, $this->apiUrl);
    }

    /**
     * Magic Method to request http verb
     * @return array
     */
    public function __call($method, $arguments) {
        $httpVerb = strtoupper($method);
        $allowedHttpVerbs = array('GET', 'POST', 'PATCH', 'DELETE', 'PUT');

        //Validate http verb
        if (in_array($httpVerb, $allowedHttpVerbs)) {
            $endPoint = $arguments[0];
            $data = isset($arguments[1]) ? $arguments[1] : array();
            return $this->request($httpVerb, $endPoint, $data);
        }

        throw new \Exception('Invalid http verb : ' . $httpVerb);
    }

    /**
     * Call MailChimp API
     * @param string $httpVerb
     * @param string $endPoint - (http://kb.mailchimp.com/api/resources)
     * @param array $data - Optional
     * @return array
     */
    public function request($httpVerb = 'GET', $endPoint, $data = array()) {
        //validate API
        if (!$this->apiKey) {
            throw new \Exception('MailChimp API Key must be set before making request!');
        }

        $endPoint = ltrim($endPoint, '/');
        $httpVerb = strtoupper($httpVerb);
        $requestUrl = $this->apiUrl . $endPoint;

        return $this->curlRequest($requestUrl, $httpVerb, $data);
    }

    /**
     * Request using curl extension
     * @param string $url
     * @param string $httpVerb
     * @param array $data - Optional
     * @return array
     */
    private function curlRequest($url, $httpVerb, array $data = array(), $curlTimeout = 15) {
        if (function_exists('curl_init') && function_exists('curl_setopt')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_USERAGENT, 'VPS/MC-API:3.0');
            curl_setopt($ch, CURLOPT_TIMEOUT, $curlTimeout);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_USERPWD, "user:" . $this->apiKey);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $httpVerb);

            //Submit data
            if (!empty($data)) {
                $jsonData = json_encode($data);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            }

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);

            return $result ? json_decode($result, true) : false;
        }

        throw new \Exception('curl extension is missing!');
    }

    public function ListCreateUpdateSubscriber($listid, $email, $leads_first_name, $leads_last_name, $leads_clientid_c, $leads_date_entered, $opportunities_date_entered, $leads_date_of_tour_start_c, $leads_date_of_tour_end_c, $leads_interest_travel_name_c, $leads_interest_travel_formula_c, $leads_interest_vehicle_c, $leads_interest_destination_web_c, $leads_primary_address_postalcode, $opportunities_assigned_user_name, $opportunities_sales_stage, $opportunities_date_closed, $product_name, $formulevoyage, $leadsourcedescription) {
        $mergefields = array();
        if (!empty($email)) {
            $md5hash = md5(strtolower($email));
            $mergefields['LNAME'] = empty($leads_last_name) ? "" : $leads_last_name;
            $mergefields['FNAME'] = empty($leads_first_name) ? "" : $leads_first_name;


            if (!empty($leads_clientid_c)) {
                $mergefields['CLIENTID'] = $leads_clientid_c;
            }
            if (!empty($leads_date_entered)) {
                $mergefields['LEADDATE'] = date('d-m-Y', strtotime($leads_date_entered));
            }
            if (!empty($opportunities_date_entered)) {
                $mergefields['OPPODATE'] = date('d-m-Y', strtotime($opportunities_date_entered));
            }
            if (!empty($leads_date_of_tour_start_c)) {
                $mergefields['TRIPSTART'] = date('d-m-Y', strtotime($leads_date_of_tour_start_c));
            }
            if (!empty($leads_date_of_tour_end_c)) {
                $mergefields['TRIPEND'] = date('d-m-Y', strtotime($leads_date_of_tour_end_c));
            }
            if (!empty($leads_interest_travel_name_c)) {
                $mergefields['PRODUCTNAM'] = $leads_interest_travel_name_c;
            }
            if (!empty($leads_interest_travel_formula_c)) {
                $mergefields['FORMULE'] = $leads_interest_travel_formula_c;
            }
            if (!empty($leads_interest_vehicle_c)) {
                $mergefields['VEHICULE'] = $leads_interest_vehicle_c;
            }
            if (!empty($leads_interest_destination_web_c)) {
                $mergefields['DESTINATIO'] = $leads_interest_destination_web_c;
            }
            if (!empty($opportunities_assigned_user_name)) {
                $mergefields['LEADASSIGN'] = $opportunities_assigned_user_name;
            }
            if (!empty($opportunities_sales_stage)) {
                $mergefields['OPPOSTATUS'] = $opportunities_sales_stage;
            }
            if (!empty($leadsourcedescription)) {
                $mergefields['LEADSOURCE'] = $leadsourcedescription;
            }
            if (!empty($opportunities_date_closed)) {
                $mergefields['OPPOEXPECT'] = date('d-m-Y', strtotime($opportunities_date_closed));
            }

            return $this->put('/lists/' . $listid . '/members/' . $md5hash, array(
                        'email_address' => $email,
                        'merge_fields' => $mergefields,
                        'status' => 'subscribed'
            ));
        }
    }

    public function ListUnsubscribe($listid, $email) {
        $md5hash = md5(strtolower($email));
        return $this->delete('/lists/' . $listid . '/members/' . $md5hash);
    }

    public function ListCreateUpdateSubscriberRestricted($listid, $email, $leads_first_name, $leads_last_name) {
        $mergefields = array();
        if (!empty($email)) {
            $md5hash = md5(strtolower($email));
            $mergefields['LNAME'] = empty($leads_last_name) ? "" : $leads_last_name;
            $mergefields['FNAME'] = empty($leads_first_name) ? "" : $leads_first_name;
            return $this->put('/lists/' . $listid . '/members/' . $md5hash, array(
                        'email_address' => $email,
                        'merge_fields' => $mergefields,
                        'status' => 'subscribed'
            ));
        }
    }
    /**
     * 
     * @param string $listid List ID where the contact will be added
     * @param string $email Email of the contact
     * @param string $leads_first_name First Name
     * @param string $leads_last_name Last Name
     * @param string $leads_clientid_c Client id
     * @param string $leads_date_entered Date of creation of the lead
     * @param string $opportunities_date_entered Date of creation of the opportunity
     * @param string $leads_date_of_tour_start_c Starting date of the Tour
     * @param string $leads_date_of_tour_end_c Ending date of the Tour
     * @param string $leads_interest_travel_name_c Travel name
     * @param string $leads_interest_travel_formula_c Travel formula
     * @param string $leads_interest_vehicle_c Vehicule name
     * @param string $leads_interest_destination_web_c Destination nalme
     * @param string $leads_primary_address_postalcode Contact Zip code
     * @param string $opportunities_assigned_user_name Opprtunity assigned Partner (username)
     * @param string $opportunities_sales_stage Opportunity Sales Stage
     * @param string $opportunities_date_closed Opportunity Closing date (or expected)
     * @param string $product_name Opportunity related product name
     * @param string $formulevoyage Travel formula
     * @param string $leadsourcedescription Lead Source description
     * @param string $subscribeNl Newsletter subscription date
     * @param string $PARTNDATE First mail sent by partner date
     * @param string $RIDERDATE First mail sent by rider date
     * @param string $lead_assigned_username Username of user assigned to lead (usuallu group_sales or group_marketing)
     * @return type result of the creation
     */
    public function ListCreateUpdateSubscriberExtended($listid, $email, $leads_first_name, $leads_last_name, $leads_clientid_c, $leads_date_entered, $opportunities_date_entered, $leads_date_of_tour_start_c, $leads_date_of_tour_end_c, $leads_interest_travel_name_c, $leads_interest_travel_formula_c, $leads_interest_vehicle_c, $leads_interest_destination_web_c, $leads_primary_address_postalcode, $opportunities_assigned_user_name, $opportunities_sales_stage, $opportunities_date_closed, $product_name, $formulevoyage, $leadsourcedescription, $subscribeNl, $PARTNDATE, $RIDERDATE,$lead_assigned_username,$reason_of_dead_opportunity) {
        $mergefields = array();
        if (!empty($email)) {
            $md5hash = md5(strtolower($email));
            
            
            if (!empty($leads_last_name)) {
                $mergefields['LNAME'] = $leads_last_name;
            }
            if (!empty($leads_first_name)) {
                $mergefields['FNAME'] = $leads_first_name;
            }

            if (!empty($leads_clientid_c)) {
                $mergefields['CLIENTID'] = $leads_clientid_c;
            }
            if (!empty($leads_date_entered)) {
                $mergefields['LEADDATE'] = date('d-m-Y', strtotime($leads_date_entered));
            }
            if (!empty($opportunities_date_entered)) {
                $mergefields['OPPODATE'] = date('d-m-Y', strtotime($opportunities_date_entered));
            }
            if (!empty($leads_date_of_tour_start_c)) {
                $mergefields['TRIPSTART'] = date('d-m-Y', strtotime($leads_date_of_tour_start_c));
            }
            if (!empty($leads_date_of_tour_end_c)) {
                $mergefields['TRIPEND'] = date('d-m-Y', strtotime($leads_date_of_tour_end_c));
            }
            if (!empty($leads_interest_travel_name_c)) {
                $mergefields['PRODUCTNAM'] = $leads_interest_travel_name_c;
            }
            if (!empty($leads_interest_travel_formula_c)) {
                $mergefields['FORMULE'] = $leads_interest_travel_formula_c;
            }
            if (!empty($leads_interest_vehicle_c)) {
                $mergefields['VEHICULE'] = $leads_interest_vehicle_c;
            }
            if (!empty($leads_interest_destination_web_c)) {
                $mergefields['DESTINATIO'] = $leads_interest_destination_web_c;
            }
            if (!empty($opportunities_assigned_user_name)) {
                $mergefields['LEADASSIGN'] = $opportunities_assigned_user_name;
            }
            if (!empty($opportunities_sales_stage)) {
                $mergefields['OPPOSTATUS'] = $opportunities_sales_stage;
            }
            if (!empty($leadsourcedescription)) {
                $mergefields['LEADSOURCE'] = $leadsourcedescription;
            }
            if (!empty($opportunities_date_closed)) {
                $mergefields['OPPOEXPECT'] = date('d-m-Y', strtotime($opportunities_date_closed));
            }
            if (!empty($subscribeNl)) {
                $mergefields['NLDATE'] = date('d-m-Y', strtotime($subscribeNl));
            }
            if (!empty($PARTNDATE)) {
                $mergefields['PARTNDATE'] = date('d-m-Y', strtotime($PARTNDATE));
            }
            if (!empty($RIDERDATE)) {
                $mergefields['RIDERDATE'] = date('d-m-Y', strtotime($RIDERDATE));
            }
            if (!empty($lead_assigned_username)) {
                $mergefields['ASSIUSER'] = $lead_assigned_username;
            }
            if (!empty($reason_of_dead_opportunity)) {
                $mergefields['REASONDEAD'] = $reason_of_dead_opportunity;
            }

            
            return $this->put('/lists/' . $listid . '/members/' . $md5hash, array(
                        'email_address' => $email,
                        'merge_fields' => $mergefields,
                        'status' => 'subscribed'
            ));
        }
    }

}
